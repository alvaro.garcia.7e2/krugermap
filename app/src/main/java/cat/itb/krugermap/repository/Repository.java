package cat.itb.krugermap.repository;

import android.net.Uri;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;

import cat.itb.krugermap.model.Animal;

public class Repository {

    private StorageReference storageRef;

    public Repository() {
        // Get a non-default Storage bucket
        FirebaseStorage storage = FirebaseStorage.getInstance("gs://my-custom-bucket");
        storageRef = storage.getReference();

    }

//    public void saveAnimal(Animal animal) {
//        //Uri animalImage= animal.getUri();
//        Uri animalImage;
//        Uri file = Uri.fromFile(new File("path/to/images/rivers.jpg"));
//        uploadTask = storageRef.putFile(file);
//
//        // Register observers to listen for when the download is done or if it fails
//        uploadTask.addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception exception) {
//                // Handle unsuccessful uploads
//            }
//        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//            @Override
//            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//                taskSnapshot.getMetadata(); //contains file metadata such as size, content-type, etc.
//
//            }
//        });
//    }

}
