package cat.itb.krugermap.screens;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cat.itb.krugermap.R;
import cat.itb.krugermap.controllers.GaleriaViewModel;

public class GaleriaFragment extends Fragment {

    private GaleriaViewModel mViewModel;

    public static GaleriaFragment newInstance() {
        return new GaleriaFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.galeria_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(GaleriaViewModel.class);
        // TODO: Use the ViewModel
    }

}
