package cat.itb.krugermap.screens;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.krugermap.R;
import cat.itb.krugermap.controllers.AvistamientoViewModel;
import cat.itb.krugermap.model.Animal;
import cat.itb.krugermap.model.Bufalo;
import cat.itb.krugermap.model.Elefante;
import cat.itb.krugermap.model.Hiena;
import cat.itb.krugermap.model.Leopardo;
import cat.itb.krugermap.model.Lion;
import cat.itb.krugermap.model.Rinoceronte;

public class AvistamientoFragment extends Fragment {

    @BindView(R.id.imageAnimal)
    ImageView imageAnimal;
    @BindView(R.id.seleccionarAnimal)
    EditText seleccionarAnimal;
    private AvistamientoViewModel mViewModel;
    private int option = 0;
    double lat;
    double lon;
    private String[] animalsToChoose = {"LEÓN", "LEOPARDO", "BÚFALO", "ELEFANTE", "RINOCERONTE", "HIENA"};

    public static AvistamientoFragment newInstance() {
        return new AvistamientoFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.avistamiento_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(AvistamientoViewModel.class);

        String ltnlng = Context.LOCATION_SERVICE;
        String[] arrayLtlng = ltnlng.split(",");
        lat = Double.parseDouble(arrayLtlng[0]);
        lon = Double.parseDouble(arrayLtlng[1]);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imageAnimal.setImageBitmap(imageBitmap);

        }
    }

    @OnClick({R.id.seleccionarAnimal, R.id.btnGoMakePhoto, R.id.bntSaveAnimal})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.seleccionarAnimal:

                new MaterialAlertDialogBuilder(getContext())
                        .setTitle("selecciona animal")
                        .setItems(animalsToChoose, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                seleccionarAnimal.setText(animalsToChoose[which]);
                                option = which;
                            }
                        })
                        .show();
                break;
            case R.id.btnGoMakePhoto:
                Intent takePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePhoto.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivityForResult(takePhoto, 1);

                }
                break;
            case R.id.bntSaveAnimal:
                switch (option){
                    case 0:
                        Animal lion = new Lion(Calendar.getInstance().getTime(), new LatLng(lat, lon), imageAnimal);
                        mViewModel.saveAnimal(lion);
                        break;
                    case 1:
                        Animal leopardo = new Leopardo(Calendar.getInstance().getTime(), new LatLng(lat, lon), imageAnimal);
                        mViewModel.saveAnimal(leopardo);
                        break;
                    case 2:
                        Animal bufalo = new Bufalo(Calendar.getInstance().getTime(), new LatLng(lat, lon), imageAnimal);
                        mViewModel.saveAnimal(bufalo);
                        break;
                    case 3:
                        Animal elefante = new Elefante(Calendar.getInstance().getTime(), new LatLng(lat, lon), imageAnimal);
                        mViewModel.saveAnimal(elefante);
                        break;
                    case 4:
                        Animal rinoceronte = new Rinoceronte(Calendar.getInstance().getTime(), new LatLng(lat, lon), imageAnimal);
                        mViewModel.saveAnimal(rinoceronte);
                        break;
                    case 5:
                        Animal hiena = new Hiena(Calendar.getInstance().getTime(), new LatLng(lat, lon), imageAnimal);
                        mViewModel.saveAnimal(hiena);
                        break;
                }

                break;
        }
    }
//    private File createImageFile() throws IOException {
//        String currentPhotoPath;
//        // Create an image file name
//        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//        String imageFileName = "JPEG_" + timeStamp + "_";
//        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
//        File image = File.createTempFile(
//                imageFileName,  /* prefix */
//                ".jpg",         /* suffix */
//                storageDir      /* directory */
//        );
//
//        // Save a file: path for use with ACTION_VIEW intents
//        currentPhotoPath = image.getAbsolutePath();
//        return image;
//    }



}
