package cat.itb.krugermap.model;

import android.media.Image;
import android.widget.ImageView;

import com.google.android.gms.maps.model.LatLng;

import java.util.Date;

public abstract class Animal {
    private Date viewdDate;
    private LatLng position;
    private String name;
    private ImageView photo;

    public ImageView getPhoto() {
        return photo;
    }

    public void setPhoto(ImageView photo) {
        this.photo = photo;
    }

    public Animal(Date viewdDate, LatLng position, ImageView photo) {
        this.viewdDate = viewdDate;
        this.position = position;
        this.name = name;
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getViewdDate() {
        return viewdDate;
    }

    public void setViewdDate(Date viewdDate) {
        this.viewdDate = viewdDate;
    }

    public LatLng getPosition() {
        return position;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }
}
