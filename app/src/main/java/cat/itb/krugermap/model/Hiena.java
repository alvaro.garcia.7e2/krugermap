package cat.itb.krugermap.model;

import android.widget.ImageView;

import com.google.android.gms.maps.model.LatLng;

import java.util.Date;

import cat.itb.krugermap.model.Animal;

public class Hiena extends Animal {
    public Hiena(Date time, LatLng latLng, ImageView imageAnimal) {
        super(time, latLng, imageAnimal);
        this.setName("Hiena");    }
}
