package cat.itb.krugermap.model;

import android.widget.ImageView;

import com.google.android.gms.maps.model.LatLng;

import java.util.Date;

public class Elefante extends Animal {
    public Elefante(Date time, LatLng latLng, ImageView imageAnimal) {
        super(time, latLng, imageAnimal);
        this.setName("Elefante");    }
}
