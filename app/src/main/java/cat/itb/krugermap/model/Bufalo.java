package cat.itb.krugermap.model;

import android.widget.ImageView;

import com.google.android.gms.maps.model.LatLng;

import java.util.Date;

public class Bufalo extends Animal {
    public Bufalo(Date time, LatLng latLng, ImageView imageAnimal) {
        super(time, latLng, imageAnimal);
        this.setName("Bufalo");    }
}
