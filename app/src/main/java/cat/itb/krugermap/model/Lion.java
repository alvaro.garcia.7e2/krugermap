package cat.itb.krugermap.model;

import android.media.Image;
import android.widget.ImageView;

import com.google.android.gms.maps.model.LatLng;

import java.util.Date;

public class Lion extends Animal {
    public Lion(Date viewdDate, LatLng position, ImageView photo) {
        super(viewdDate, position, photo);
        this.setName("Lion");
    }
}
